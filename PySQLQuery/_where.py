#!/usr/bin/env python3

from sqlalchemy.sql.expression import or_


class Where:

    _query = None
    _filter = None
    _or = False
    _conditions = list()
    _condition = list()

    def __init__(self, query, filter_):
        self._query = query
        self._filter = filter_

    def or_(self, filter_):
        self._or = True
        self._filter = filter_
        return self

    def and_(self, filter_):
        self._append_conditions()
        self._filter = filter_
        return self

    def equals(self, arg):
        return self._apend_contition(self._filter == arg)

    def not_equals(self, arg):
        return self._apend_contition(self._filter != arg)

    def bigger(self, arg):
        return self._apend_contition(self._filter > arg)

    def bigger_or_equals(self, arg):
        return self._apend_contition(self._filter >= arg)

    def lower(self, arg):
        return self._apend_contition(self._filter < arg)

    def lower_or_equals(self, arg):
        return self._apend_contition(self._filter <= arg)

    def like(self, arg: str):
        return self._apend_contition(self._filter.like(arg))

    def in_(self, arg: list):
        return self._apend_contition(self._filter.in_(arg))

    def not_in(self, arg: list):
        return self._apend_contition(~self._filter.in_(arg))

    def is_null(self):
        return self._apend_contition(self._filter.is_(None))

    def is_not_null(self):
        return self._apend_contition(self._filter.isnot(None))

    def end_where(self):
        self._append_conditions()
        self._query.query = self._query.query.filter(*self._conditions)
        return self._query

    def _apend_contition(self, condition):
        self._condition.append(condition)
        return self

    def _append_conditions(self):
        if self._or:
            self._conditions.append(or_(*self._condition))
            self._or = False
        elif len(self._condition) > 0:
            self._conditions.append(*self._condition)

        self._condition = list()
