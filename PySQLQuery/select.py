#!/usr/bin/env python3

from ._where import Where
from personalCalc.core.database.instance import DB


class Select:

    query = None

    def __init__(self, to_select):
        to_select = [to_select] if type(to_select) != list else to_select
        self.query = DB.session.query(*to_select)

    def where(self, condition):
        return Where(self, condition)

    def order_by(self, condition):
        self.query = self.query.order_by(condition)
        return self

    def one(self):
        return self.query.one()

    def many(self, number, start=0):
        return self.query[start:number]

    def all(self):
        return self.query.all()
