#!/usr/bin/env python3

from personalCalc.core.database.instance import DB


class Insert:

    @staticmethod
    def one(object_):
        DB.session.add(object_)
        DB.session.commit()
        return object_

    @staticmethod
    def many(objects):
        DB.session.add_all(objects)
        DB.session.commit()
        return objects
